package com.example.tugas2b;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView; // Tambahkan import untuk TextView

import androidx.appcompat.app.AppCompatActivity;

public class Activity2 extends AppCompatActivity {

    TextView textViewPertanyaan1, textViewPertanyaan2, textViewProdi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_2);

        textViewPertanyaan1 = findViewById(R.id.textViewPertanyaan1);
        textViewPertanyaan2 = findViewById(R.id.textViewPertanyaan2);
        textViewProdi = findViewById(R.id.textViewProdi);

        Intent intent = getIntent();
        String pertanyaan1 = intent.getStringExtra("pertanyaan1");
        String pertanyaan2 = intent.getStringExtra("pertanyaan2");
        String prodi = intent.getStringExtra("prodi");

        textViewPertanyaan1.setText("Apakah kamu mahasiswa FILKOM? " + pertanyaan1);
        textViewPertanyaan2.setText("Prodi: " + pertanyaan2);
        textViewProdi.setText("Prodi: " + prodi);
    }
}